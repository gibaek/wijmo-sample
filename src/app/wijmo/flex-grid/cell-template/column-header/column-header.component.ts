import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-column-header',
    templateUrl: './column-header.component.html',
    styleUrls: ['./column-header.component.css']
})
export class ColumnHeaderComponent implements OnInit {
    isFrozen: boolean;
    isHeader: boolean;
    @Input('header') header;
    @Input('columnIndex') columnIndex;
    @Input('currentSort') currentSort;
    @Input('columnFixed') columnFixed;
    @Input('fixedIndex') fixedIndex;

    @Output() onSort: EventEmitter<any> = new EventEmitter();
    @Output() onFrozen: EventEmitter<any> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
        this.isFrozen = this.columnIndex >= this.fixedIndex ? false : true;
        this.isHeader = this.header === 'ck' ? true : false;
    }

    onSortClick() {
        this.onSort.emit(this.columnIndex);
    }

    onFrozenClick() {
        this.isFrozen = !this.isFrozen;
        this.onFrozen.emit(this.columnIndex);
    }
}
