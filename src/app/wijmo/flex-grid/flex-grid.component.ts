import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import * as wjcCore from 'wijmo/wijmo';

import {ColumnLayout} from './column-layout';
import {WjFlexGrid} from 'wijmo/wijmo.angular2.grid';

@Component({
    selector: 'app-flex-grid',
    templateUrl: './flex-grid.component.html',
})
export class FlexGridComponent implements OnInit {


    @ViewChild('flexGrid') flexGrid: WjFlexGrid;
    @Input('data') data: any[];
    @Input('gridName') gridName: string;
    @Input('isReadOnly') isReadOnly: boolean;
    @Input('rowHeader') rowHeader: boolean;
    @Input('columnFixed') columnFixed: boolean;
    @Input('hasHeader') hasHeader: boolean;

    @Output() addData: EventEmitter<string> = new EventEmitter<string>();

    gridData: wjcCore.CollectionView;
    columnLayout: any[];
    isChecked: boolean;
    scrollCnt = -1;
    hoverRow = -1;
    fixedIndex: number = 0;

    constructor() {
    }

    ngOnInit() {
        /*그리드에 데이터 넣기*/
        this.gridData = new wjcCore.CollectionView(this.data);
        /*컬럼 레이아웃 설정*/
        this.loadColumnLayout();
        console.log(this.columnLayout);
        /*컬럼명 고정*/
        this.flexGrid.frozenRows = 0;

    }

    initialized() {
        /*순서 중요*/
        this.setHeader();
        this.loadFrozenColumn();
        this.loadSort();
    }

    setHeader() {
        if (this.hasHeader) {
            this.flexGrid.frozenColumns = 1;
            this.flexGrid.columns[0].allowResizing = false;
            this.flexGrid.columns[0].allowDragging = false;
            this.flexGrid.columns[0].isReadOnly = false;

            for (let i = 1; i < this.flexGrid.columns.length; i++) {
                this.flexGrid.columns[i].isReadOnly = true;
            }
            console.log('columns', this.flexGrid.columns[0].isReadOnly);
        }
    }

    saveSort() {
        const sortStates: any[] = [];
        const description = this.flexGrid.collectionView.sortDescriptions[0];
        sortStates.push({
            property: description.property,
            ascending: description.ascending,
        });
        console.log('Saved Sort Sate: ' + JSON.stringify(sortStates));
        localStorage[this.gridName + '-sort'] = JSON.stringify(sortStates);
    }

    loadSort() {
        if (localStorage[this.gridName + '-sort']) {
            const sortStates = JSON.parse(localStorage[this.gridName + '-sort']);
            for (const sortState of sortStates) {
                console.log('load sort ' + sortState.property + ', ' + sortState.ascending);
                const sd = new wjcCore.SortDescription(sortState.property, sortState.ascending);
                this.flexGrid.collectionView.sortDescriptions.push(sd);
            }
        }
    }

    mouseMove(e) {
        let ht = this.flexGrid.hitTest(e);
        let r = ht.row;
        if (r != this.hoverRow) {
            if (this.hoverRow > -1) {
                this.flexGrid.rows[this.hoverRow].cssClass = null;
            }
            this.hoverRow = r;
            if (this.hoverRow > -1) {
                this.flexGrid.rows[this.hoverRow].cssClass = 'hoverRow';
            }
        }
        // let r = ht.cellType === wjcGrid.CellType.Cell ? ht.row : -1;

    }

    mouseLeave() {
        if (this.hoverRow > -1) {
            this.flexGrid.rows[this.hoverRow].cssClass = null;
            this.hoverRow = -1;
        }
    }

    scrollPositionChanged() {
        // console.log(this.flexGrid.viewRange.bottomRow);
        if (this.flexGrid.viewRange.bottomRow >= this.flexGrid.rows.length - 1) {
            this.onAddDataClick('+');
            this.flexGrid.collectionView.refresh();
            this.flexGrid.scrollIntoView(90, 90);
            this.scrollCnt++;
            console.log('scroll cnt', this.scrollCnt);
        }

        if (this.flexGrid.viewRange.bottomRow < 25) {
            this.onAddDataClick('-');
            if (this.scrollCnt >= 0) {
                this.flexGrid.collectionView.refresh();
                this.flexGrid.scrollIntoView(110, 110);
                this.scrollCnt--;
            }
            console.log('scroll cnt', this.scrollCnt);
        }
    }

    loadColumnLayout() {
        if (localStorage) {
            if (localStorage[this.gridName + '-columnLayout']) {
                this.columnLayout = JSON.parse(localStorage[this.gridName + '-columnLayout']);
            } else {
                this.columnLayout = ColumnLayout[this.gridName];
            }
        }
    }

    saveColumnLayout() {
        if (localStorage) {
            const tmpColumnLayout = JSON.parse(this.flexGrid.columnLayout)['columns'];
            for (let i = 0; i < this.columnLayout.length; i++) {
                tmpColumnLayout[i].visible = this.flexGrid.columns.getColumn(tmpColumnLayout[i].binding).visible;
            }
            localStorage[this.gridName + '-columnLayout'] = JSON.stringify(tmpColumnLayout);
        }
    }

    loadFrozenColumn() {
        if (localStorage[this.gridName + '-frozen']) {
            this.flexGrid.frozenColumns = +localStorage[this.gridName + '-frozen'];
            this.fixedIndex = +localStorage[this.gridName + '-frozen'];
        }
    }

    onSortClick(e) {
        let sd: wjcCore.SortDescription;
        if (this.flexGrid.columns[e].currentSort === '+') {
            sd = new wjcCore.SortDescription(this.flexGrid.columns[e].binding, false);
        } else if (this.flexGrid.columns[e].currentSort === '-') {
            sd = new wjcCore.SortDescription(this.flexGrid.columns[e].binding, true);
        } else {
            sd = new wjcCore.SortDescription(this.flexGrid.columns[e].binding, true);
        }
        this.flexGrid.collectionView.sortDescriptions.clear();
        this.flexGrid.collectionView.sortDescriptions.push(sd);
    }

    onFrozenClick(columnindex) {
        if (this.columnFixed) {
            if (columnindex >= this.flexGrid.frozenColumns) {
                /*set frozn*/
                this.flexGrid.columns.moveElement(columnindex, this.flexGrid.frozenColumns);
                this.flexGrid.frozenColumns = this.flexGrid.frozenColumns + 1;
                this.fixedIndex++;

            } else {
                /*unset*/
                console.log('unset');
                this.flexGrid.columns.moveElement(columnindex, this.flexGrid.frozenColumns - 1);
                this.flexGrid.frozenColumns = this.flexGrid.frozenColumns - 1;
                this.fixedIndex--;
            }
            /*save*/
            if (localStorage) {
                localStorage[this.gridName + '-frozen'] = this.flexGrid.frozenColumns;
                console.log('save frozenColumns', this.flexGrid.frozenColumns);
            }
            this.saveColumnLayout();
        }
    }

    onAddDataClick(symbol: string) {
        this.addData.emit(symbol);
    }

    checkAll() {
        const rowHeader = this.flexGrid.hostElement.querySelector('.wj-rowheaders');
        this.isChecked = !this.isChecked;
        for (let i = 0; i < this.flexGrid.rows.length; i++) {
            this.flexGrid.rows[i].isSelected = this.isChecked;
            // rowHeader.children[i].getElementsByTagName('input')[0].checked = this.isChecked;
        }
    }

    checkOne($event: Event) {
        const ht = this.flexGrid.hitTest($event);
        console.log(ht);
        this.flexGrid.rows[ht._row].isSelected = !this.flexGrid.rows[ht._row].isSelected;
    }

    onCloumnHeaderChecked(cloumnHeader: HTMLInputElement) {

    }

    onRowHeaderChange(index: number) {
    }
}
