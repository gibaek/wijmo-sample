import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexGridComponent} from './flex-grid/flex-grid.component';
import {WjGridModule} from 'wijmo/wijmo.angular2.grid';
import {WjCoreModule} from 'wijmo/wijmo.angular2.core';
import {ColumnHeaderComponent} from './flex-grid/cell-template/column-header/column-header.component';
import {WjGridDetailModule} from 'wijmo/wijmo.angular2.grid.detail';

@NgModule({
    imports: [
        CommonModule,
        WjGridModule,
        WjCoreModule,
        WjGridDetailModule
    ],
    declarations: [FlexGridComponent, ColumnHeaderComponent],
    exports: [FlexGridComponent]
})
export class WijmoModule {
}
