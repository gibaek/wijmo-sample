import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Sample1Component} from './component/sample1/sample1.component';
import {Sample2Component} from './component/sample2/sample2.component';
import {Sample3Component} from './component/sample3/sample3.component';
import {Sample4Component} from './component/sample4/sample4.component';


const appRoutes: Routes = [
    {path: '', redirectTo: 'sample1', pathMatch: 'full'},
    {path: 'sample1', component: Sample1Component},
    {path: 'sample2', component: Sample2Component},
    {path: 'sample3', component: Sample3Component},
    {path: 'sample4', component: Sample4Component},

];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        )

    ],
    exports: [RouterModule]
})
export class AppRouterModule {
}
