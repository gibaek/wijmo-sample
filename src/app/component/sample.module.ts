import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Sample2Component} from './sample2/sample2.component';
import {Sample1Component} from './sample1/sample1.component';
import {SampleDataService} from '../service/sample-data.service';
import {SampleApiService} from '../service/sample-api.service';
import {WijmoModule} from '../wijmo/wijmo.module';
import {Sample3Component} from './sample3/sample3.component';
import {ApiGatewayService} from '../service/api-gateway.service';
import {HttpModule} from '@angular/http';
import {Sample4Component} from './sample4/sample4.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        WijmoModule,
        HttpModule,
        HttpClientModule
    ],
    declarations: [
        Sample1Component,
        Sample2Component,
        Sample3Component,
        Sample4Component
    ],
    providers: [SampleDataService, SampleApiService, ApiGatewayService],
})
export class SampleModule {
}
