import {Component, OnInit} from '@angular/core';
import {SampleDataService} from '../../service/sample-data.service';

@Component({
    selector: 'app-sample2',
    templateUrl: './sample2.component.html',
    styleUrls: ['./sample2.component.css']
})
export class Sample2Component implements OnInit {
    data1 = [];
    data2 = [];
    gridName = 'sample2'

    constructor(private _dataSrv: SampleDataService) {
        this.data1 = _dataSrv.getSampleData2();
    }

    ngOnInit() {
    }


    toRight() {

    }

    toLeft() {

    }
}
