
import { Component, OnInit } from '@angular/core';
import { ApiGatewayService } from '../../service/api-gateway.service';
import { Observable } from 'rxjs/Observable';
import { Company_Admin } from '../../models/company_admin';

import { SampleApiService } from '../../service/sample-api.service';
import { Tables } from './datatables';
@Component({
    selector: 'app-sample3',
    templateUrl: './sample3.component.html',
    styleUrls: ['./sample3.component.css']
})
export class Sample3Component implements OnInit {
    loginMessageSvc: string;
    loginMessageUsr: string;
    tables = Tables;
    data;
    selectTable;
    selectServer = 'svc-api';
    constructor(private _apiService: ApiGatewayService, private com_admin: Company_Admin) {
        console.log(this.tables);
    }

    ngOnInit() {
    }
    setTable(value) {
        console.log(value);
        this.selectTable = value;
    }
    goLoginSvc() {
        this.selectServer = 'svc-api';
        this._apiService.loginSvc('id=samillee&pwd=241&ccode=231').subscribe(res => this.loginMessageSvc = res.result);
    }
    goLoginUsr() {
        this.selectServer = 'user-api';
        this._apiService.loginUsr('id=samillee&pwd=241&ccode=231').subscribe(res => this.loginMessageUsr = res.result);
    }
    getTable(query: string, select: string) {
        if (select === 'svc') {
            return this._apiService.get(`svc-api/tables/${query}`).map(response => response.json());

        } else {
            return this._apiService.get(`user-api/tables/${query}`).map(response => response.json());
            // .subscribe(res => this.data = JSON.stringify(res));
        }
    }
    getTest() {
    }
    tableLike() {
    }
    parentTable() {
    }
    tablesInfo() {
        this._apiService.get(this.selectServer + `/tablesinfo`).subscribe(res => console.log(res));
    }
    talbeCount() {
        this._apiService.count(`company_admins`).subscribe(res => console.log(res));
    }
    table() {
        // this._apiService.genericGet<Company_Admin[]>(this.selectServer + `/tables/company_admin`).subscribe((res: Company_Admin[]) => {
        //     console.log('dsa');
        //     return res;
        // });
        this._apiService.genGet<Company_Admin[]>(this.selectServer + `/tables/${this.selectTable}`).subscribe((res: Company_Admin[]) => {
            console.log('dsa', res as Company_Admin[]);
            return res;
        });
    }
    tableColumns() {
        this._apiService.get(this.selectServer + `/tables/${this.selectTable}-columns`).subscribe(res => console.log(res));
    }
    tableId() {
        this._apiService.get(this.selectServer + `/tables/${this.selectTable}/241`).subscribe(res => console.log(res));
    }
    tableOrderBy() {
        this._apiService.get(this.selectServer + `/tables/${this.selectTable}/?orderBy="nm"&equalTo='이삼일'&limitToLast=5`)
            .subscribe(res => console.log(res));
    }
    lookupId() {
        this._apiService.get(this.selectServer + `/tables/${this.selectTable}-lookup-id/241`).subscribe(res => console.log(res));
    }
    lookupName() {
        this._apiService.get(this.selectServer + `/tables/${this.selectTable}-lookup-name/이사일`).subscribe(res => console.log(res));
    }

    shallow() {
        this._apiService.get(this.selectServer + `/tables/${this.selectTable}?shallow=true`).subscribe(res => console.log(res));
    }
    whereTable() {
        this._apiService.postWhere(this.selectServer + `/tables/${this.selectTable}-where`, `nm = '이삼일' and id='231' `)
            .subscribe(res => console.log(res));
    }
    countWhereTable() {
        this._apiService.postWhere(this.selectServer + `/tables/${this.selectTable}-count-where`, `nm = '이삼일' and id='231' `)
            .subscribe(res => console.log(res));
    }
    countWhereColumnsTable() {
        this._apiService.postWhere(this.selectServer + `/tables/${this.selectTable}-column-where/nm`, `nm = '이삼일' and id='231' `)
            .subscribe(res => console.log(res));
    }
    lookPost() {
        this._apiService.postWhere(this.selectServer + `/tables/${this.selectTable}-lookup-id`, ['231', '241'])
            .subscribe(res => console.log(res));
    }
    putTableId() {
        const _id = '233';
        this._apiService.put(this.selectServer + `/tables/${this.selectTable}/${_id}`, { id: _id, nm: '이사오' })
            .subscribe(res => console.log(res));
    }
    putMultiTableId() {
        const data = [
            {
                'id': '231',
                'nm': '이삼일'
            },
            {
                'id': '241',
                'nm': '이사일'
            }
        ]
            ;
        this._apiService.put(this.selectServer + `/tables/company_admin`, data)
            .subscribe(res => console.log(res));
    }
    delTableId() {
        const _id = '233';
        this._apiService.delete(this.selectServer + `/tables/company_admin/${_id}`)
            .subscribe(res => console.log(res));
    }
    delMultiTableId() {
        const data = [
            '233', '234'
        ]
            ;
        this._apiService.delete(this.selectServer + `/tables/company_admin`, data)
            .subscribe(res => console.log(res));
    }
    creatTable() {
        const _id = '244';
        const data = {
            'id': _id,
            'nm': '이삼일',
            'nameKana': 'IsmNx',
            'localeId': Number(_id),
            'langCode': 'IsmNx',
            'login':
            {
                'loginName': 'IsmNx',
                'pwd': 'IsmNx',
                'pwdUpdatedAt': '2016-09-09T00:00:00+09:00',
                'lastLoginAt': '2016-09-09T00:00:00+09:00',
                'failCount': 0,
                'isAccountLocked': true,
                'lockedAt': '2016-09-09T00:00:00+09:00'
            },
            'accountCode': 'IsmNx',
            'authorityType': 'IsmNx',
            'toManageApp': true,
            'isTaxAccountant': true,
            'toManageOrg': true,
            'toSynchronize': true,
            'applyMailAddress': 'IsmNx',
            'ismAuthMethodCode': 'ISM',
            'freeItem1': '',
            'freeItem2': '',
            'freeItem3': '',
            'freeItem4': '',
            'freeItem5': '',
            'accountCreatorCode': 'IsmNx',
            'periodCode': '1',
            'filterUpdatedAt': '2016-09-09T00:00:00+09:00',
            'islogArciveFirsttime': true,
            'lastAlertlogAt': '2016-09-09T00:00:00+09:00',
            'audit':
            {
                'creator': 'master',
                'updater': 'administrator',
                'createdAt': '2016-09-09T00:00:00+09:00',
                'updatedAt': '2016-09-09T00:00:00+09:00',
                'oId': 'KNTEST0000'
            }
        };


        this._apiService.postWhere(this.selectServer + `/tables/company_admin/${_id}`, data)
            .subscribe(res => console.log(res));
    }
}
