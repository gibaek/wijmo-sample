export const Tables = [
    'aos_account',
    'aos_client_registerstatus',
    'aos_gaecmdrequestlog',
    'aos_gaecompanyreqlog',
    'dtb_aos_apkfile',
    'clientstatus',
    'clientstatus_fde',
    'clientstatus_invupdate',
    'clientstatus_securityng',
    'client',
    'client_appver_important',
    'client_contracthardware',
    'client_deregistered',
    'client_devicelocationlog',
    'client_invverprofile',
    'client_logonuser',
    'client_nogood',
    'client_printer',
    'client_printer_mib',
    'client_printerassign',
    'client_printerfavorite',
    'client_rcconnectip',
    'client_user',
    'client_winupdate',
    'cmd',
    'cmd_adminsendmail',
    'cmdfailed',
    'cmdstatus',
    'updatedclient',
    'updatedcompany',
    'updatedism', 'client_2_contract',
    'client_2_license',
    'aos_appportalapk',
    'config_company',
    'config_console',
    'config_grouppolicy',
    'config_opelogcollect',
    'config_rc',
    'config_secdiag',
    'config_servicetemplate',
    'config_stocktaking',
    'configclient_appautoupdate',
    ' configclient_exclude',
    'configclient_printercollect',
    'configclient_winupdate',
    'configgroup_aos_appportal',
    'configgroup_exclude',
    'configgroup_ios_appportal',
    'confignoti_alert',
    'confignoti_aos_init',
    'confignoti_ios',
    'confignoti_opelogalert',
    'confignoti_stocktaking',
    'confignoti_yaraialert',
    'configset_custominstaller',
    'configset_fdeinstaller',
    'configset_license',
    'configset_windeletefolder',
    'db_iosvpp_webapiurl',
    'ios_appportalapp',
    'iosvpp_account',
    'iosvpp_accout2client',
    'iosvpp_license',
    'iosvpp_licenserelated',
    'iosvpp_listupdate',
    'iosvpp_tokenconfig',
    'custom_certificate',
    'custom_mdmapp',
    'dtb_clientstatus',
    'dtb_companyconfig',
    'dtb_config',
    'dtb_groupstatus',
    'dtb_registry',
    'em_config',
    'em_connectlog',
    'em_useapply',
    'group',
    'group_filter',
    'group_filtered_client',
    'hwinv',
    'hwinvaos',
    'hwinvios',
    'invconfig_aos_app3rdemergencyctrl',
    'invconfig_assetinfo',
    'invconfig_assetinfooption',
    'invconfig_diaglinktorectify',
    'invconfig_emc',
    'invconfig_emregistration',
    'invconfig_inifile',
    'invconfig_invbase',
    'invconfig_printercollect',
    'invconfig_registry',
    'invconfig_restrictsw',
    'invconfig_sw',
    'invconfigclientversion',
    'invconfigupdate',
    'addoninv_appverdiag',
    'addoninv_assetinfo',
    'addoninv_hotfix',
    'addoninv_inifile',
    'addoninv_invbasediag',
    'addoninv_quality',
    'addoninv_registry',
    'clientstatus_apns',
    'ios_apnscert',
    'ios_profile',
    'ios_profileinv',
    'ios_securityinv',
    'company_commkey',
    'ism_admin',
    'ism_apiopelog',
    'ism_apioprequest',
    'ism_clientinstaller',
    'ism_companyconfig2',
    'ism_companyopelog',
    'ism_contracthistory',
    'ism_contractnotifylog',
    'ism_contractservice',
    'ism_logapserverreqlog',
    'ism_opelogserver',
    'ism_opelogserver2company',
    'ism_optionproduct',
    'ism_relatedcompany',
    'ism_servermachine',
    'ism_serverschedule',
    'ism_systemconfig',
    'ism_systemconfig2',
    'ism_systemstatus',
    'ism_tempfile',
    'ism_yaraiserver',
    'option_sux_agent',
    'option_sux_coordinaterequest',
    'option_sux_server',
    'company',
    'company_admin',
    'clientstatus_plugindic',
    ' db_plugindic',
    'dbmeta_locale',
    'db_adobe',
    'db_aososlocale',
    'db_clientinstallerfilename',
    'db_latestappver',
    'db_office',
    'db_restrictsw_category',
    'db_restrictsw_categorysw',
    'db_restrictsw_masterfile',
    'db_restrictsw_swfilecustom',
    'db_winosmaster',
    'nw_ip',
    'nw_wifi',
    'nw_wifi2client',
    'logpolicy',
    'policy',
    'policyclient_ios_setting',
    ' policyclient_setting',
    'policygroup_setting',
    'config_invsendresult',
    'summary_adobe',
    'summary_client',
    'summary_diagnosis',
    'summary_invregistry',
    'summary_listapp',
    'summary_listswfile',
    'summary_office',
    'summaryqueue',
    'swinv_adobe',
    'swinv_antivirus',
    'swinv_aos_app',
    'swinv_app',
    'swinv_autoupdateset',
    'swinv_ie',
    'swinv_ios_appinv',
    'swinv_ios_mdmapp',
    'swinv_office',
    'swinv_os',
    'swinv_swfile',
    'swinvcompany_swfile',
    'ui_alertlog',
    'ui_alertlogsearch',
    'ui_clientsubmenu',
    'ui_dashboarduserconfig',
    'ui_displaygroup',
    'ui_displayitem',
    'ui_displayitem2group',
    'ui_groupassetinfofiltered',
    'ui_groupfilterupdatestatus',
    'ui_list',
    'ui_notice',
    'ui_notice2company',
    'ui_noticeread',
    'ui_providefunc',
    'ui_screen',
    'ui_screenfavorite',
];
