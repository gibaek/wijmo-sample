import {Component, OnInit} from '@angular/core';
import {SampleDataService} from '../../service/sample-data.service';

@Component({
    selector: 'app-sample4',
    templateUrl: './sample4.component.html',
    styleUrls: ['./sample4.component.css']
})
export class Sample4Component implements OnInit {

    data: any[];
    dataLan = 200;
    gridName: string;
    maxLan: number;
    offset: number;
    limit: number;

    constructor(private _sampleDataService: SampleDataService) {
        this.data = _sampleDataService.getSampleScrollData(this.dataLan);
        this.gridName = 'sample1';
        this.maxLan = 777;
    }

    ngOnInit() {
    }

    numCal(symbol: string): boolean {
        if (symbol === '+') {
            this.dataLan += 100;
            if (this.dataLan > this.maxLan) {
                this.dataLan = this.maxLan;
            }
            return true;
        } else if (symbol === '-') {
            if (this.dataLan > 200) {
                this.dataLan -= 100;
                return true;
            } else {
                return false;
            }
        }
    }

    addData(symbol: string) {
        console.log('addData', symbol);
        if (this.numCal(symbol)) {
            const more = this._sampleDataService.getSampleScrollData(this.dataLan);
            this.data.length = 0;
            for (let i = 0; i < more.length; i++) {
                this.data.push(more[i]);
            }
            console.log('data.length', this.data.length);
        }
    }

}
