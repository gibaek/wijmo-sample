import {TestBed, inject} from '@angular/core/testing';

import {SampleApiService} from './sample-api.service';

describe('SampleApiService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [SampleApiService]
        });
    });

    it('should be created', inject([SampleApiService], (service: SampleApiService) => {
        expect(service).toBeTruthy();
    }));
});
