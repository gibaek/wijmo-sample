import { Observable } from 'rxjs/Observable';
import { Company_Admin } from './../models/company_admin';
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptionsArgs } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

const JSON_MAPPER_FN = res => res.json();

@Injectable()
export class ApiGatewayService {

    reqOptions: RequestOptionsArgs = {};
    reqOptionsLogin: RequestOptionsArgs = {};

    apiUrl = 'http://172.30.1.48:1203/consoleapp';
    apiVer = 'v1';

    constructor(private _http: Http, private _httpClient: HttpClient) {
        this._makeLoginHttpOption();
        this._makeDefaultHttpOption();
    }

    private _makeDefaultHttpOption() {
        const headerInfo = new Headers();
        headerInfo.set('Authorization', 'Bearer 9b04d152-845e-c0a3-7839-4003c96da594');
        // headerInfo.set('Access-Control-Allow-Origin', '*');
        this.reqOptions.headers = headerInfo;
        this.reqOptions.withCredentials = true;
    }

    private _makeLoginHttpOption() {
        const headerInfo = new Headers();
        headerInfo.set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        headerInfo.set('Accept', 'application/json, text/plain, */*');
        this.reqOptionsLogin.headers = headerInfo;
        this.reqOptionsLogin.withCredentials = true;
    }

    loginSvc(data) {
        return this._http.post(`${this.apiUrl}/${this.apiVer}/auth/svc-api/login/local`, data, this.reqOptionsLogin).map(JSON_MAPPER_FN);
    }
    loginUsr(data) {
        console.log(`${this.apiUrl}/${this.apiVer}/auth/user-api/login/local`);
        console.log(data);
        return this._http.post(`${this.apiUrl}/${this.apiVer}/auth/user-api/login/local`, data, this.reqOptionsLogin).map(JSON_MAPPER_FN);
    }

    get(url: string) {
        console.log(`${this.apiUrl}/${this.apiVer}/${url}`);
        return this._http.get(`${this.apiUrl}/${this.apiVer}/${url}`, this.reqOptions).map(JSON_MAPPER_FN);
    }
    genGet<T>(url: string): Observable<T> {
        console.log(`${this.apiUrl}/${this.apiVer}/${url}`);
        return this._http.get(`${this.apiUrl}/${this.apiVer}/${url}`, this.reqOptions).map(JSON_MAPPER_FN);
    }
    genericGet<T>(url: string): Observable<T> {
        const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        headers.set('Authorization', 'Bearer 9b04d152-845e-c0a3-7839-4003c96da594');
        headers.set('Accept', 'application/json, text/plain, */*');
        console.log(`${this.apiUrl}/${this.apiVer}/${url}`);
        return this._httpClient.get<T>(`${this.apiUrl}/${this.apiVer}/${url}`, { headers, withCredentials: true });
    }
    post(url: string, data: any) {
        return this._http.post(`${this.apiUrl}/${this.apiVer}/${url}`, data, this.reqOptions).map(JSON_MAPPER_FN);
    }

    postWhere(url: string, data: any) {
        console.log(`${this.apiUrl}/${this.apiVer}/${url}`);
        console.log(data);
        return this._http.post(`${this.apiUrl}/${this.apiVer}/${url}`, data, this.reqOptions).map(JSON_MAPPER_FN);
    }

    put(url: string, data: any) {
        return this._http.put(`${this.apiUrl}/${this.apiVer}/${url}`, data, this.reqOptions).map(JSON_MAPPER_FN);
    }

    delete(url: string, data?: any) {
        return this._http.delete(`${this.apiUrl}/${this.apiVer}/${url}`, this.reqOptions).map(JSON_MAPPER_FN);
    }
    count(url: string) {
        return this._http.get(`http://172.30.1.48:1203/count/${url}`, this.reqOptions).map(JSON_MAPPER_FN);
    }

}
