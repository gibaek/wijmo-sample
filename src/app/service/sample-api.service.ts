import {Injectable} from '@angular/core';
import {ApiGatewayService} from './api-gateway.service';

@Injectable()
export class SampleApiService {

    constructor(private _apiService: ApiGatewayService) {
    }

    goLoginSvc(id:string, pwd:string) {
        return this._apiService.loginSvc(`id=${id}&pwd=${pwd}&ccode=231`);
    }

    goLoginUsr(id:string, pwd:string, ccode:string) {
        return this._apiService.loginUsr(`id=${id}&pwd=${pwd}&ccode=${ccode}`);
    }

    getTable(query: string, orderBy: string, equalsTo:string, limitToLast:string, select: string) {
        if (select === 'svc') {
            console.log('query:', `svc-api/tables/${query}?orderBy="${orderBy}"&equalTo='${equalsTo}'&limitToLast=${limitToLast}`);
            return this._apiService.get(`svc-api/tables/${query}?orderBy="${orderBy}"&equalTo='${equalsTo}'&limitToLast=${limitToLast}`);
        } else if(select === 'usr') {
            console.log('query:', `svc-api/tables/${query}?orderBy="${orderBy}"&equalTo='${equalsTo}'&limitToLast=${limitToLast}`);
            return this._apiService.get(`user-api/tables/${query}?orderBy="${orderBy}"&equalTo='${equalsTo}'&limitToLast=${limitToLast}`);
        }
    }

    getCompanyAdmin (select2: string) {
        if(select2 ==='usr') {
            return this._apiService.get(`user-api/tables/company_admin?orderBy="nm"&equalTo='이삼일'&limitToLast=5`);
        }else if(select2 === 'svc'){
            return this._apiService.get(`svc-api/tables/company_admin?orderBy="nm"&equalTo='이삼일'&limitToLast=5`);
        }
    }





}
