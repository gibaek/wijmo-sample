import {Injectable} from '@angular/core';

@Injectable()
export class SampleDataService {
    getSampleData2() {
        const data = [],
            code = 'KNTEST0000,CCR2000313,NI1000000'.split(','),
            name = 'kntest,顧客CCR2000313,顧客NI1000000'.split(','),
            template = 'ISM CloudOne';
        for (let i = 0; i < 3; i++) {
            data.push({
                code: code[i],
                name: name[i],
                template: template
            });
        }
        return data;
    }

    getSampleScrollData(dataLan: number): any[] {
        let startNum: number;
        if (dataLan === 200) {
            startNum = 0;
        } else {
            startNum = dataLan - 200;
        }
        console.log('start', startNum);
        console.log('last', dataLan);
        const data = [],
            countries = 'US,Germany,UK,Japan,Italy,Greece,Canada'.split(','),
            products = 'Widget,Gadget,Doohickey'.split(','),
            colors = 'Black,White,Red,Green,Blue'.split(','),
            dt = new Date();
        for (let i = startNum; i < dataLan; i++) {
            data.push({
                id: i,
                date: new Date(dt.getFullYear(), i % 12, 25, i % 24, i % 60, i % 60).toString(),
                country: countries[Math.floor(Math.random() * countries.length)],
                product: products[Math.floor(Math.random() * products.length)],
                color: colors[Math.floor(Math.random() * colors.length)],
                amount: Math.round(Math.random() * 10000) - 5000,
                ck: false
            });
        }
        return data;
    }

}
