﻿export interface IModelBase {
}

export class ModelBase implements IModelBase {
}

export class ModelBase_POCO extends ModelBase {
    id: string;
    // name: string;
    // note: string;

    // Server send as this flag set if the record is realy only
    // The client cannot change ID and isReadOnly
    isReadOnly: boolean;
    isDeleted: boolean;

    // Ng App set the following info available only in the UI
    lastUpate: Date;
    isModified: boolean;
    isNew: boolean;
    selected: boolean;

    static getHash(from: ModelBase_POCO): string {
        let content: string;

        content = content + from.id;
        // content = content + from.name;
        // content = content + from.note;
        content = content + from.isReadOnly;
        return content;
    }

    static getNew(from: ModelBase_POCO): ModelBase_POCO {
        let to = new ModelBase_POCO();

        // to.name = '1_' + from.name;

        return to;
    }
}


export class ModelBase_POCO2 extends ModelBase {
    id: number;
    // name: string;
    // note: string;

    // Server send as this flag set if the record is realy only
    // The client cannot change ID and isReadOnly
    isReadOnly: boolean;
    isDeleted: boolean;

    // Ng App set the following info available only in the UI
    lastUpate: Date;
    isModified: boolean;
    isNew: boolean;
    selected: boolean;

    static getHash(from: ModelBase_POCO): string {
        let content: string;

        content = content + from.id;
        // content = content + from.name;
        // content = content + from.note;
        content = content + from.isReadOnly;
        return content;
    }

    static getNew(from: ModelBase_POCO): ModelBase_POCO {
        let to = new ModelBase_POCO();

        // to.name = '1_' + from.name;

        return to;
    }
}
