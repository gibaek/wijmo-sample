import { Company_Admin } from './models/company_admin';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRouterModule} from './app-router.module';
import {SampleModule} from './component/sample.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        SampleModule,
        AppRouterModule
    ],
    providers: [Company_Admin],
    bootstrap: [AppComponent]
})
export class AppModule {
}
