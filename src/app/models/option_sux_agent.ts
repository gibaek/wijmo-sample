﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {Ism_OptionProduct} from './ism_optionproduct';
import {Option_Sux_Server} from './option_sux_server';
import {AuditType} from './audittype';

/// <summary>
/// StratusphereUX agent data: license only
/// </summary>
export class Option_Sux_Agent extends ModelBase_POCO {

    id: string;     // Uuid Md5(SuxServerId+SuxAgentCode)
    suxServerId: string;
    suxAgentCode: string;     // See code table
    agentHash: string;
    optionProductId: string;
    audit: AuditType;
    optionProduct: Ism_OptionProduct;
    suxSrvInfo: Option_Sux_Server;

    static GetHash(from: Option_Sux_Agent): string {
        let content: string;

        content = content + from.id;
        content = content + from.suxServerId;
        content = content + from.suxAgentCode;
        content = content + from.agentHash;
        content = content + from.optionProductId;

        return content;
    }

    static GetNew(from: Option_Sux_Agent): Option_Sux_Agent {
        let to: Option_Sux_Agent = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: Option_Sux_Agent): boolean {
        if (to.id !== this.id)
            return false;
        if (to.suxServerId !== this.suxServerId)
            return false;
        if (to.suxAgentCode !== this.suxAgentCode)
            return false;
        if (to.agentHash !== this.agentHash)
            return false;
        if (to.optionProductId !== this.optionProductId)
            return false;

        return true;
    }

    clone(): Option_Sux_Agent {
        let copied: Option_Sux_Agent = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
