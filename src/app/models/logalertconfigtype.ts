﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {ModelBase_POCO, ModelBase} from "../shared/model-base";

export class LogAlertConfigType extends ModelBase {

    constructor() {
        super();
    }

    onLog: boolean;
    onAlert: boolean;
    alertCode: string;     // See code table
    onExclude: boolean;

    equals(to: LogAlertConfigType): boolean {
        if (to.onLog !== this.onLog)
            return false;
        if (to.onAlert !== this.onAlert)
            return false;
        if (to.alertCode !== this.alertCode)
            return false;
        if (to.onExclude !== this.onExclude)
            return false;

        return true;
    }

    clone(): LogAlertConfigType {
        let copied = new LogAlertConfigType();

        copied.onLog = this.onLog;
        copied.onAlert = this.onAlert;
        copied.alertCode = this.alertCode;
        copied.onExclude = this.onExclude;

        return copied;
    }

    static getNew(from: LogAlertConfigType): LogAlertConfigType {
        let to = from.clone();

        return to;
    }
}
