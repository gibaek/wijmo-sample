﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO2} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

/// <summary>
/// Ism managed office
/// </summary>
export class Db_Office extends ModelBase_POCO2 {

    id: number;
    productName: string;
    ver: number;
    swSuiteTypeCode: string;     // See code table
    swLicenseTypeCode: string;     // See code table
    priority: number;
    msBundleProductCode_A: string;     // See code table
    audit: AuditType;

    static GetHash(from: Db_Office): string {
        let content: string;

        content = content + from.id;
        content = content + from.productName;
        content = content + from.ver;
        content = content + from.swSuiteTypeCode;
        content = content + from.swLicenseTypeCode;
        content = content + from.priority;
        content = content + from.msBundleProductCode_A;

        return content;
    }

    static GetNew(from: Db_Office): Db_Office {
        let to: Db_Office = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        return to;
    }

    constructor() {
        super();
        this.isNew = true;
    }

    equals(to: Db_Office): boolean {
        if (to.id !== this.id)
            return false;
        if (to.productName !== this.productName)
            return false;
        if (to.ver !== this.ver)
            return false;
        if (to.swSuiteTypeCode !== this.swSuiteTypeCode)
            return false;
        if (to.swLicenseTypeCode !== this.swLicenseTypeCode)
            return false;
        if (to.priority !== this.priority)
            return false;
        if (to.msBundleProductCode_A !== this.msBundleProductCode_A)
            return false;

        return true;
    }

    clone(): Db_Office {
        let copied: Db_Office = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
