﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

export class SwInv_Ios_AppInv extends ModelBase_POCO {

    id: string;     // Unique(CId + Name + Ver + PackageName ) or Unique(CId+SeqNo)
    oId: string;
    cId: string;
    identifier: string;
    nm: string;     // Name
    ver: string;
    shortVer: string;
    bundleSize: number;
    dynamicSize: number;
    onRestrictApp: boolean;
    audit: AuditType;

    static GetHash(from: SwInv_Ios_AppInv): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.cId;
        content = content + from.identifier;
        content = content + from.nm;
        content = content + from.ver;
        content = content + from.shortVer;
        content = content + from.bundleSize;
        content = content + from.dynamicSize;
        content = content + from.onRestrictApp;

        return content;
    }

    static GetNew(from: SwInv_Ios_AppInv): SwInv_Ios_AppInv {
        let to: SwInv_Ios_AppInv = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: SwInv_Ios_AppInv): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.cId !== this.cId)
            return false;
        if (to.identifier !== this.identifier)
            return false;
        if (to.nm !== this.nm)
            return false;
        if (to.ver !== this.ver)
            return false;
        if (to.shortVer !== this.shortVer)
            return false;
        if (to.bundleSize !== this.bundleSize)
            return false;
        if (to.dynamicSize !== this.dynamicSize)
            return false;
        if (to.onRestrictApp !== this.onRestrictApp)
            return false;

        return true;
    }

    clone(): SwInv_Ios_AppInv {
        let copied: SwInv_Ios_AppInv = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
