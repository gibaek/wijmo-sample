﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

export class InvConfigClientVersion extends ModelBase_POCO {

    id: string;
    oId: string;
    swInvConfigAt: Date;
    registryInvConfigAt: Date;
    inifileInvConfigAt: Date;
    assetInfoConfigAt: Date;
    restrictSwConfigAt: Date;
    questionInvConfigVer: Date;
    winUpdateConfigAt: Date;
    audit: AuditType;

    static GetHash(from: InvConfigClientVersion): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.swInvConfigAt;
        content = content + from.registryInvConfigAt;
        content = content + from.inifileInvConfigAt;
        content = content + from.assetInfoConfigAt;
        content = content + from.restrictSwConfigAt;
        content = content + from.questionInvConfigVer;
        content = content + from.winUpdateConfigAt;

        return content;
    }

    static GetNew(from: InvConfigClientVersion): InvConfigClientVersion {
        let to: InvConfigClientVersion = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: InvConfigClientVersion): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.swInvConfigAt !== this.swInvConfigAt)
            return false;
        if (to.registryInvConfigAt !== this.registryInvConfigAt)
            return false;
        if (to.inifileInvConfigAt !== this.inifileInvConfigAt)
            return false;
        if (to.assetInfoConfigAt !== this.assetInfoConfigAt)
            return false;
        if (to.restrictSwConfigAt !== this.restrictSwConfigAt)
            return false;
        if (to.questionInvConfigVer !== this.questionInvConfigVer)
            return false;
        if (to.winUpdateConfigAt !== this.winUpdateConfigAt)
            return false;

        return true;
    }

    clone(): InvConfigClientVersion {
        let copied: InvConfigClientVersion = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
