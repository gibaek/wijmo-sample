﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {ModelBase_POCO, ModelBase} from "../shared/model-base";

export class PolicyAosPwdType extends ModelBase {

    constructor() {
        super();
    }

    onPassPolicy: boolean;
    pwdComplexCode: string;     // See code table
    validityPeriod: number;
    minWordCount: number;
    maxTrialCount: number;
    rockTime: number;
    onChangePassMsg: boolean;
    changePassMsg: string;

    equals(to: PolicyAosPwdType): boolean {
        if (to.onPassPolicy !== this.onPassPolicy)
            return false;
        if (to.pwdComplexCode !== this.pwdComplexCode)
            return false;
        if (to.validityPeriod !== this.validityPeriod)
            return false;
        if (to.minWordCount !== this.minWordCount)
            return false;
        if (to.maxTrialCount !== this.maxTrialCount)
            return false;
        if (to.rockTime !== this.rockTime)
            return false;
        if (to.onChangePassMsg !== this.onChangePassMsg)
            return false;
        if (to.changePassMsg !== this.changePassMsg)
            return false;

        return true;
    }

    clone(): PolicyAosPwdType {
        let copied = new PolicyAosPwdType();

        copied.onPassPolicy = this.onPassPolicy;
        copied.pwdComplexCode = this.pwdComplexCode;
        copied.validityPeriod = this.validityPeriod;
        copied.minWordCount = this.minWordCount;
        copied.maxTrialCount = this.maxTrialCount;
        copied.rockTime = this.rockTime;
        copied.onChangePassMsg = this.onChangePassMsg;
        copied.changePassMsg = this.changePassMsg;

        return copied;
    }

    static getNew(from: PolicyAosPwdType): PolicyAosPwdType {
        let to = from.clone();

        return to;
    }
}
