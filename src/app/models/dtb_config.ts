﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {Dtb_Aos_ApkFile} from './dtb_aos_apkfile';
import {Dtb_ClientStatus} from './dtb_clientstatus';
import {Custom_Certificate} from './custom_certificate';
import {Dtb_Registry} from './dtb_registry';
import {AuditType} from './audittype';
import {DtbWinType} from './dtbwintype';
import {DtbIosType} from './dtbiostype';

/// <summary>
/// Dtb tables renovated; This is a set of distribution items
/// </summary>
/// <LongDescription>
/// Merged OptionProductDistribute; IosDistributionConfig; ArdDistributionConfig; DistributionUpdateList
/// </LongDescription>
export class Dtb_Config extends ModelBase_POCO {

    id: string;
    oId: string;
    dtbMainTypeCode: string;     // See code table
    nm: string;     // Name
    startTime: Date;
    summary: string;

    // Json
    dtbWin_Json: string;
    dtbWin_Json_Object: DtbWinType;

    // Json
    dtbIos_Json: string;
    dtbIos_Json_Object: DtbIosType;
    aosApkId: string;
    updateStatusCode: string;     // See code table
    custom_CertificateId: string;
    dtb_RegistryId: string;     // DtbId
    audit: AuditType;
    dtb_Aos_ApkFile: Dtb_Aos_ApkFile;
    clientDtbStatus: Dtb_ClientStatus[];
    custom_Certificate: Custom_Certificate;
    registry: Dtb_Registry;

    static GetHash(from: Dtb_Config): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.dtbMainTypeCode;
        content = content + from.nm;
        content = content + from.startTime;
        content = content + from.summary;
        content = content + from.dtbWin_Json;
        content = content + from.dtbIos_Json;
        content = content + from.aosApkId;
        content = content + from.updateStatusCode;
        content = content + from.custom_CertificateId;
        content = content + from.dtb_RegistryId;

        return content;
    }

    static GetNew(from: Dtb_Config): Dtb_Config {
        let to: Dtb_Config = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: Dtb_Config): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.dtbMainTypeCode !== this.dtbMainTypeCode)
            return false;
        if (to.nm !== this.nm)
            return false;
        if (to.startTime !== this.startTime)
            return false;
        if (to.summary !== this.summary)
            return false;
        if (to.dtbWin_Json !== this.dtbWin_Json)
            return false;
        if (to.dtbIos_Json !== this.dtbIos_Json)
            return false;
        if (to.aosApkId !== this.aosApkId)
            return false;
        if (to.updateStatusCode !== this.updateStatusCode)
            return false;
        if (to.custom_CertificateId !== this.custom_CertificateId)
            return false;
        if (to.dtb_RegistryId !== this.dtb_RegistryId)
            return false;

        return true;
    }

    clone(): Dtb_Config {
        let copied: Dtb_Config = JSON.parse(JSON.stringify(this));

        return copied;
    }


    fromJsonString() {
        this.dtbWin_Json_Object = JSON.parse(this.dtbWin_Json);
        this.dtbIos_Json_Object = JSON.parse(this.dtbIos_Json);
    }

    toJsonString() {
        this.dtbWin_Json = JSON.stringify(this.dtbWin_Json_Object);
        this.dtbIos_Json = JSON.stringify(this.dtbIos_Json_Object);
    }
}
