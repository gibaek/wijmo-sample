﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {MsgConfigType} from './msgconfigtype';
import {AuditType} from './audittype';

/// <summary>
/// OId
/// </summary>
export class ConfigNoti_Ios extends ModelBase_POCO {

    id: string;     // OId
    befExpireNoticeDays: number;
    toRepeatBefExpireNotice: boolean;
    befExpire: MsgConfigType;
    expire: MsgConfigType;
    register: MsgConfigType;
    toAutoInstallClientProgram: boolean;     // TRUE: Automatically install iOS client program after installing MDM configuration profile
    audit: AuditType;

    static GetHash(from: ConfigNoti_Ios): string {
        let content: string;

        content = content + from.id;
        content = content + from.befExpireNoticeDays;
        content = content + from.toRepeatBefExpireNotice;
        content = content + from.befExpire; // todo to confirm it's right for complex type
        content = content + from.expire; // todo to confirm it's right for complex type
        content = content + from.register; // todo to confirm it's right for complex type
        content = content + from.toAutoInstallClientProgram;

        return content;
    }

    static GetNew(from: ConfigNoti_Ios): ConfigNoti_Ios {
        let to: ConfigNoti_Ios = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: ConfigNoti_Ios): boolean {
        if (to.id !== this.id)
            return false;
        if (to.befExpireNoticeDays !== this.befExpireNoticeDays)
            return false;
        if (to.toRepeatBefExpireNotice !== this.toRepeatBefExpireNotice)
            return false;
        if (!to.befExpire.equals(this.befExpire))
            return false;
        if (!to.expire.equals(this.expire))
            return false;
        if (!to.register.equals(this.register))
            return false;
        if (to.toAutoInstallClientProgram !== this.toAutoInstallClientProgram)
            return false;

        return true;
    }

    clone(): ConfigNoti_Ios {
        let copied: ConfigNoti_Ios = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
