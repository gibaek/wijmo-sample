﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';
import {LogPolicySystemType} from './logpolicysystemtype';
import {LogPolicyEmType} from './logpolicyemtype';
import {LogPolicyFileOpeType} from './logpolicyfileopetype';
import {LogPolicyWebAccessType} from './logpolicywebaccesstype';
import {LogAlertConfigType} from './logalertconfigtype';
import {LogPolicyRunPcType} from './logpolicyrunpctype';
import {LogPolicyProcType} from './logpolicyproctype';
import {LogPolicyPrintType} from './logpolicyprinttype';
import {LogPolicySendStopType} from './logpolicysendstoptype';

/// <summary>
/// Operation log policy config
/// </summary>
/// <LongDescription>
/// Merged all LogPolicy* tables
/// </LongDescription>
export class LogPolicy extends ModelBase_POCO {

    id: string;
    oId: string;
    logPolicyTypeCode: string;     // See code table
    nm: string;     // Name

    // Json
    systemAlert_Json: string;
    systemAlert_Json_Object: LogPolicySystemType;

    // Json
    em_Json: string;
    em_Json_Object: LogPolicyEmType;

    // Json
    fileOpe_Json: string;
    fileOpe_Json_Object: LogPolicyFileOpeType;

    // Json
    webAccess_Json: string;
    webAccess_Json_Object: LogPolicyWebAccessType;

    // Json
    webmail_Json: string;
    webmail_Json_Object: LogAlertConfigType;

    // Json
    runPc_Json: string;
    runPc_Json_Object: LogPolicyRunPcType;

    // Json
    proc_Json: string;
    proc_Json_Object: LogPolicyProcType;

    // Json
    print_Json: string;
    print_Json_Object: LogPolicyPrintType;

    // Json
    logSendStop_Json: string;
    logSendStop_Json_Object: LogPolicySendStopType;
    updaterName: string;     // Last updater name
    hash: string;     // Name
    audit: AuditType;

    static GetHash(from: LogPolicy): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.logPolicyTypeCode;
        content = content + from.nm;
        content = content + from.systemAlert_Json;
        content = content + from.em_Json;
        content = content + from.fileOpe_Json;
        content = content + from.webAccess_Json;
        content = content + from.webmail_Json;
        content = content + from.runPc_Json;
        content = content + from.proc_Json;
        content = content + from.print_Json;
        content = content + from.logSendStop_Json;
        content = content + from.updaterName;
        content = content + from.hash;

        return content;
    }

    static GetNew(from: LogPolicy): LogPolicy {
        let to: LogPolicy = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: LogPolicy): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.logPolicyTypeCode !== this.logPolicyTypeCode)
            return false;
        if (to.nm !== this.nm)
            return false;
        if (to.systemAlert_Json !== this.systemAlert_Json)
            return false;
        if (to.em_Json !== this.em_Json)
            return false;
        if (to.fileOpe_Json !== this.fileOpe_Json)
            return false;
        if (to.webAccess_Json !== this.webAccess_Json)
            return false;
        if (to.webmail_Json !== this.webmail_Json)
            return false;
        if (to.runPc_Json !== this.runPc_Json)
            return false;
        if (to.proc_Json !== this.proc_Json)
            return false;
        if (to.print_Json !== this.print_Json)
            return false;
        if (to.logSendStop_Json !== this.logSendStop_Json)
            return false;
        if (to.updaterName !== this.updaterName)
            return false;
        if (to.hash !== this.hash)
            return false;

        return true;
    }

    clone(): LogPolicy {
        let copied: LogPolicy = JSON.parse(JSON.stringify(this));

        return copied;
    }


    fromJsonString() {
        this.systemAlert_Json_Object = JSON.parse(this.systemAlert_Json);
        this.em_Json_Object = JSON.parse(this.em_Json);
        this.fileOpe_Json_Object = JSON.parse(this.fileOpe_Json);
        this.webAccess_Json_Object = JSON.parse(this.webAccess_Json);
        this.webmail_Json_Object = JSON.parse(this.webmail_Json);
        this.runPc_Json_Object = JSON.parse(this.runPc_Json);
        this.proc_Json_Object = JSON.parse(this.proc_Json);
        this.print_Json_Object = JSON.parse(this.print_Json);
        this.logSendStop_Json_Object = JSON.parse(this.logSendStop_Json);
    }

    toJsonString() {
        this.systemAlert_Json = JSON.stringify(this.systemAlert_Json_Object);
        this.em_Json = JSON.stringify(this.em_Json_Object);
        this.fileOpe_Json = JSON.stringify(this.fileOpe_Json_Object);
        this.webAccess_Json = JSON.stringify(this.webAccess_Json_Object);
        this.webmail_Json = JSON.stringify(this.webmail_Json_Object);
        this.runPc_Json = JSON.stringify(this.runPc_Json_Object);
        this.proc_Json = JSON.stringify(this.proc_Json_Object);
        this.print_Json = JSON.stringify(this.print_Json_Object);
        this.logSendStop_Json = JSON.stringify(this.logSendStop_Json_Object);
    }
}
