﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO2} from '../shared/model-base';

/// <summary>
/// Locales Ism supports
/// </summary>
export class DbMeta_Locale extends ModelBase_POCO2 {

    id: number;     // LocaleId
    suffix: string;

    static GetHash(from: DbMeta_Locale): string {
        let content: string;

        content = content + from.id;
        content = content + from.suffix;

        return content;
    }

    static GetNew(from: DbMeta_Locale): DbMeta_Locale {
        let to: DbMeta_Locale = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        return to;
    }

    constructor() {
        super();
        this.isNew = true;
    }

    equals(to: DbMeta_Locale): boolean {
        if (to.id !== this.id)
            return false;
        if (to.suffix !== this.suffix)
            return false;

        return true;
    }

    clone(): DbMeta_Locale {
        let copied: DbMeta_Locale = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
