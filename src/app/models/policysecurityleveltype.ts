﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {ModelBase_POCO, ModelBase} from "../shared/model-base";

export class PolicySecurityLevelType extends ModelBase {

    constructor() {
        super();
    }

    onHotfix: boolean;
    onAvEngineVer: boolean;     // Antivirus Engine Version Check

    equals(to: PolicySecurityLevelType): boolean {
        if (to.onHotfix !== this.onHotfix)
            return false;
        if (to.onAvEngineVer !== this.onAvEngineVer)
            return false;

        return true;
    }

    clone(): PolicySecurityLevelType {
        let copied = new PolicySecurityLevelType();

        copied.onHotfix = this.onHotfix;
        copied.onAvEngineVer = this.onAvEngineVer;

        return copied;
    }

    static getNew(from: PolicySecurityLevelType): PolicySecurityLevelType {
        let to = from.clone();

        return to;
    }
}
