﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

/// <summary>
/// Communication secrete keys
/// </summary>
export class Company_CommKey extends ModelBase_POCO {

    id: string;     // OId
    secretkey: string;
    publicKey: string;
    apiAuthKey: string;
    audit: AuditType;

    static GetHash(from: Company_CommKey): string {
        let content: string;

        content = content + from.id;
        content = content + from.secretkey;
        content = content + from.publicKey;
        content = content + from.apiAuthKey;

        return content;
    }

    static GetNew(from: Company_CommKey): Company_CommKey {
        let to: Company_CommKey = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: Company_CommKey): boolean {
        if (to.id !== this.id)
            return false;
        if (to.secretkey !== this.secretkey)
            return false;
        if (to.publicKey !== this.publicKey)
            return false;
        if (to.apiAuthKey !== this.apiAuthKey)
            return false;

        return true;
    }

    clone(): Company_CommKey {
        let copied: Company_CommKey = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
