﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {Ui_GroupAssetInfoFiltered} from './ui_groupassetinfofiltered';
import {Group_Filtered_Client} from './group_filtered_client';
import {AuditType} from './audittype';

export class Group_Filter extends ModelBase_POCO {

    id: string;     // 0 for entire company, 1 for group not set, other for a group
    oId: string;
    filterId: string;
    filterName: string;
    condXml: string;
    toClientListUpdate: boolean;
    clientListUpdatedAt: Date;
    toAutoUpdate: boolean;
    filterTargetCode: string;     // See code table
    uId: string;
    displayOrder: number;
    toShow: boolean;
    audit: AuditType;
    anyInvConfItemRelatedFilters: Ui_GroupAssetInfoFiltered[];
    groupClients: Group_Filtered_Client[];

    static GetHash(from: Group_Filter): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.filterId;
        content = content + from.filterName;
        content = content + from.condXml;
        content = content + from.toClientListUpdate;
        content = content + from.clientListUpdatedAt;
        content = content + from.toAutoUpdate;
        content = content + from.filterTargetCode;
        content = content + from.uId;
        content = content + from.displayOrder;
        content = content + from.toShow;

        return content;
    }

    static GetNew(from: Group_Filter): Group_Filter {
        let to: Group_Filter = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: Group_Filter): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.filterId !== this.filterId)
            return false;
        if (to.filterName !== this.filterName)
            return false;
        if (to.condXml !== this.condXml)
            return false;
        if (to.toClientListUpdate !== this.toClientListUpdate)
            return false;
        if (to.clientListUpdatedAt !== this.clientListUpdatedAt)
            return false;
        if (to.toAutoUpdate !== this.toAutoUpdate)
            return false;
        if (to.filterTargetCode !== this.filterTargetCode)
            return false;
        if (to.uId !== this.uId)
            return false;
        if (to.displayOrder !== this.displayOrder)
            return false;
        if (to.toShow !== this.toShow)
            return false;

        return true;
    }

    clone(): Group_Filter {
        let copied: Group_Filter = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
