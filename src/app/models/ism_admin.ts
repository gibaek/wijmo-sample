﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AccountLoginType} from './accountlogintype';
import {AuditType} from './audittype';

/// <summary>
/// Admin Console login account info
/// </summary>
export class Ism_Admin extends ModelBase_POCO {

    id: string;
    nm: string;     // Name
    langCode: string;     // ja;ko;en;zh ...
    login: AccountLoginType;
    templateId: number;
    audit: AuditType;

    static GetHash(from: Ism_Admin): string {
        let content: string;

        content = content + from.id;
        content = content + from.nm;
        content = content + from.langCode;
        content = content + from.login; // todo to confirm it's right for complex type
        content = content + from.templateId;

        return content;
    }

    static GetNew(from: Ism_Admin): Ism_Admin {
        let to: Ism_Admin = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: Ism_Admin): boolean {
        if (to.id !== this.id)
            return false;
        if (to.nm !== this.nm)
            return false;
        if (to.langCode !== this.langCode)
            return false;
        if (!to.login.equals(this.login))
            return false;
        if (to.templateId !== this.templateId)
            return false;

        return true;
    }

    clone(): Ism_Admin {
        let copied: Ism_Admin = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
