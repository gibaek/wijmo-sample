﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {Option_Sux_Agent} from './option_sux_agent';
import {AuditType} from './audittype';

export class Ism_OptionProduct extends ModelBase_POCO {

    id: string;
    optionProductActionCode: string;     // See code table
    displayName: string;     // Japanse

    // Json
    displayName_i18n: string;
    displayName_i18n_Object: any; // Map<string, string>;
    summary: string;

    // Json
    summary_i18n: string;
    summary_i18n_Object: any; // Map<string, string>;
    filepath: string;
    filename: string;
    ver: string;
    fileSize: number;
    isDeleted: boolean;
    optionProductCode: string;     // See code table
    absoluteFilepath: string;
    downloadUrl: string;
    rcClientTypeCode: string;     // See code table
    fdeClientTypeCode: string;     // See code table
    audit: AuditType;
    suxAgents: Option_Sux_Agent[];

    static GetHash(from: Ism_OptionProduct): string {
        let content: string;

        content = content + from.id;
        content = content + from.optionProductActionCode;
        content = content + from.displayName;
        content = content + from.displayName_i18n;
        content = content + from.summary;
        content = content + from.summary_i18n;
        content = content + from.filepath;
        content = content + from.filename;
        content = content + from.ver;
        content = content + from.fileSize;
        content = content + from.optionProductCode;
        content = content + from.absoluteFilepath;
        content = content + from.downloadUrl;
        content = content + from.rcClientTypeCode;
        content = content + from.fdeClientTypeCode;

        return content;
    }

    static GetNew(from: Ism_OptionProduct): Ism_OptionProduct {
        let to: Ism_OptionProduct = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: Ism_OptionProduct): boolean {
        if (to.id !== this.id)
            return false;
        if (to.optionProductActionCode !== this.optionProductActionCode)
            return false;
        if (to.displayName !== this.displayName)
            return false;
        if (to.displayName_i18n !== this.displayName_i18n)
            return false;
        if (to.summary !== this.summary)
            return false;
        if (to.summary_i18n !== this.summary_i18n)
            return false;
        if (to.filepath !== this.filepath)
            return false;
        if (to.filename !== this.filename)
            return false;
        if (to.ver !== this.ver)
            return false;
        if (to.fileSize !== this.fileSize)
            return false;
        if (to.optionProductCode !== this.optionProductCode)
            return false;
        if (to.absoluteFilepath !== this.absoluteFilepath)
            return false;
        if (to.downloadUrl !== this.downloadUrl)
            return false;
        if (to.rcClientTypeCode !== this.rcClientTypeCode)
            return false;
        if (to.fdeClientTypeCode !== this.fdeClientTypeCode)
            return false;

        return true;
    }

    clone(): Ism_OptionProduct {
        let copied: Ism_OptionProduct = JSON.parse(JSON.stringify(this));

        return copied;
    }


    fromJsonString() {
        this.displayName_i18n_Object = JSON.parse(this.displayName_i18n);
        this.summary_i18n_Object = JSON.parse(this.summary_i18n);
    }

    toJsonString() {
        this.displayName_i18n = JSON.stringify(this.displayName_i18n_Object);
        this.summary_i18n = JSON.stringify(this.summary_i18n_Object);
    }
}
