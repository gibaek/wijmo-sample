﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

export class HwInvIos extends ModelBase_POCO {

    id: string;
    oId: string;
    udid: string;
    phone: string;
    wifiMac: number;
    bluetoothMac: number;
    imei: string;     // if GSM supported
    meid: string;     // if CDMA supported
    deviceName: string;
    osVer: string;
    buildVer: string;
    modelName: string;     // iPhone
    model: string;     // E.g. MC606J
    productName: string;     //  iPhone3.1
    serialNo: string;
    deviceCapacityGB: number;     // GBytes
    availableDeviceCapacity: number;
    batteryLevel: number;
    cellTechCode: string;     // See code table
    modemFirmwareVer: string;
    iccid: string;
    currentCarrierNw: string;
    simCarrierNetwork: string;
    carrierSettingsVer: string;
    isVoiceRoaming: boolean;
    isDataRoaming: boolean;
    isRoaming: boolean;
    subscriberMcc: string;
    subscriberMnc: string;
    currentMcc: string;
    currentMnc: string;
    onInvRetry: boolean;
    audit: AuditType;

    static GetHash(from: HwInvIos): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.udid;
        content = content + from.phone;
        content = content + from.wifiMac;
        content = content + from.bluetoothMac;
        content = content + from.imei;
        content = content + from.meid;
        content = content + from.deviceName;
        content = content + from.osVer;
        content = content + from.buildVer;
        content = content + from.modelName;
        content = content + from.model;
        content = content + from.productName;
        content = content + from.serialNo;
        content = content + from.deviceCapacityGB;
        content = content + from.availableDeviceCapacity;
        content = content + from.batteryLevel;
        content = content + from.cellTechCode;
        content = content + from.modemFirmwareVer;
        content = content + from.iccid;
        content = content + from.currentCarrierNw;
        content = content + from.simCarrierNetwork;
        content = content + from.carrierSettingsVer;
        content = content + from.isVoiceRoaming;
        content = content + from.isDataRoaming;
        content = content + from.isRoaming;
        content = content + from.subscriberMcc;
        content = content + from.subscriberMnc;
        content = content + from.currentMcc;
        content = content + from.currentMnc;
        content = content + from.onInvRetry;

        return content;
    }

    static GetNew(from: HwInvIos): HwInvIos {
        let to: HwInvIos = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: HwInvIos): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.udid !== this.udid)
            return false;
        if (to.phone !== this.phone)
            return false;
        if (to.wifiMac !== this.wifiMac)
            return false;
        if (to.bluetoothMac !== this.bluetoothMac)
            return false;
        if (to.imei !== this.imei)
            return false;
        if (to.meid !== this.meid)
            return false;
        if (to.deviceName !== this.deviceName)
            return false;
        if (to.osVer !== this.osVer)
            return false;
        if (to.buildVer !== this.buildVer)
            return false;
        if (to.modelName !== this.modelName)
            return false;
        if (to.model !== this.model)
            return false;
        if (to.productName !== this.productName)
            return false;
        if (to.serialNo !== this.serialNo)
            return false;
        if (to.deviceCapacityGB !== this.deviceCapacityGB)
            return false;
        if (to.availableDeviceCapacity !== this.availableDeviceCapacity)
            return false;
        if (to.batteryLevel !== this.batteryLevel)
            return false;
        if (to.cellTechCode !== this.cellTechCode)
            return false;
        if (to.modemFirmwareVer !== this.modemFirmwareVer)
            return false;
        if (to.iccid !== this.iccid)
            return false;
        if (to.currentCarrierNw !== this.currentCarrierNw)
            return false;
        if (to.simCarrierNetwork !== this.simCarrierNetwork)
            return false;
        if (to.carrierSettingsVer !== this.carrierSettingsVer)
            return false;
        if (to.isVoiceRoaming !== this.isVoiceRoaming)
            return false;
        if (to.isDataRoaming !== this.isDataRoaming)
            return false;
        if (to.isRoaming !== this.isRoaming)
            return false;
        if (to.subscriberMcc !== this.subscriberMcc)
            return false;
        if (to.subscriberMnc !== this.subscriberMnc)
            return false;
        if (to.currentMcc !== this.currentMcc)
            return false;
        if (to.currentMnc !== this.currentMnc)
            return false;
        if (to.onInvRetry !== this.onInvRetry)
            return false;

        return true;
    }

    clone(): HwInvIos {
        let copied: HwInvIos = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
