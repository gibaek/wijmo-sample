﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {LinkInfoType} from './linkinfotype';
import {AuditType} from './audittype';

/// <summary>
/// Corrective action link for various diagnosis results; Need to enhance!!!
/// </summary>
export class InvConfig_DiagLinkToRectify extends ModelBase_POCO {

    id: string;     // OId
    hotfix: LinkInfoType;
    antivirus: LinkInfoType;
    restrictSw: LinkInfoType;
    appVer: LinkInfoType;
    invBase: LinkInfoType;
    audit: AuditType;

    static GetHash(from: InvConfig_DiagLinkToRectify): string {
        let content: string;

        content = content + from.id;
        content = content + from.hotfix; // todo to confirm it's right for complex type
        content = content + from.antivirus; // todo to confirm it's right for complex type
        content = content + from.restrictSw; // todo to confirm it's right for complex type
        content = content + from.appVer; // todo to confirm it's right for complex type
        content = content + from.invBase; // todo to confirm it's right for complex type

        return content;
    }

    static GetNew(from: InvConfig_DiagLinkToRectify): InvConfig_DiagLinkToRectify {
        let to: InvConfig_DiagLinkToRectify = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: InvConfig_DiagLinkToRectify): boolean {
        if (to.id !== this.id)
            return false;
        if (!to.hotfix.equals(this.hotfix))
            return false;
        if (!to.antivirus.equals(this.antivirus))
            return false;
        if (!to.restrictSw.equals(this.restrictSw))
            return false;
        if (!to.appVer.equals(this.appVer))
            return false;
        if (!to.invBase.equals(this.invBase))
            return false;

        return true;
    }

    clone(): InvConfig_DiagLinkToRectify {
        let copied: InvConfig_DiagLinkToRectify = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
