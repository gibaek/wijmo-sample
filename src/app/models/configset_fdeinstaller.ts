﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

/// <summary>
/// ChekPoint Full Disk Encryption (FDE)
/// </summary>
export class ConfigSet_FdeInstaller extends ModelBase_POCO {

    id: string;
    oId: string;
    updateId: string;
    displayName: string;

    // Json
    displayName_i18n: string;
    displayName_i18n_Object: any; // Map<string, string>;
    summary: string;

    // Json
    summary_i18n: string;
    summary_i18n_Object: any; // Map<string, string>;
    filename: string;
    ver: string;
    fileSize: number;
    regAt: Date;
    audit: AuditType;

    static GetHash(from: ConfigSet_FdeInstaller): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.updateId;
        content = content + from.displayName;
        content = content + from.displayName_i18n;
        content = content + from.summary;
        content = content + from.summary_i18n;
        content = content + from.filename;
        content = content + from.ver;
        content = content + from.fileSize;
        content = content + from.regAt;

        return content;
    }

    static GetNew(from: ConfigSet_FdeInstaller): ConfigSet_FdeInstaller {
        let to: ConfigSet_FdeInstaller = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: ConfigSet_FdeInstaller): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.updateId !== this.updateId)
            return false;
        if (to.displayName !== this.displayName)
            return false;
        if (to.displayName_i18n !== this.displayName_i18n)
            return false;
        if (to.summary !== this.summary)
            return false;
        if (to.summary_i18n !== this.summary_i18n)
            return false;
        if (to.filename !== this.filename)
            return false;
        if (to.ver !== this.ver)
            return false;
        if (to.fileSize !== this.fileSize)
            return false;
        if (to.regAt !== this.regAt)
            return false;

        return true;
    }

    clone(): ConfigSet_FdeInstaller {
        let copied: ConfigSet_FdeInstaller = JSON.parse(JSON.stringify(this));

        return copied;
    }


    fromJsonString() {
        this.displayName_i18n_Object = JSON.parse(this.displayName_i18n);
        this.summary_i18n_Object = JSON.parse(this.summary_i18n);
    }

    toJsonString() {
        this.displayName_i18n = JSON.stringify(this.displayName_i18n_Object);
        this.summary_i18n = JSON.stringify(this.summary_i18n_Object);
    }
}
