﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {ModelBase_POCO, ModelBase} from "../shared/model-base";

export class MetaDbInfoType extends ModelBase {

    constructor() {
        super();
    }

    versionMajor: number;
    versionMinor: number;
    versionRev: number;
    installedAt: Date;

    equals(to: MetaDbInfoType): boolean {
        if (to.versionMajor !== this.versionMajor)
            return false;
        if (to.versionMinor !== this.versionMinor)
            return false;
        if (to.versionRev !== this.versionRev)
            return false;
        if (to.installedAt !== this.installedAt)
            return false;

        return true;
    }

    clone(): MetaDbInfoType {
        let copied = new MetaDbInfoType();

        copied.versionMajor = this.versionMajor;
        copied.versionMinor = this.versionMinor;
        copied.versionRev = this.versionRev;
        copied.installedAt = this.installedAt;

        return copied;
    }

    static getNew(from: MetaDbInfoType): MetaDbInfoType {
        let to = from.clone();

        return to;
    }
}
