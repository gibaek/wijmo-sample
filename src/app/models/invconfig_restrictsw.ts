﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

/// <summary>
/// Messages to display
/// </summary>
export class InvConfig_RestrictSw extends ModelBase_POCO {

    id: string;     // OId
    onExecCtrl: boolean;
    toDisplayMessage: boolean;
    message: string;
    audit: AuditType;

    static GetHash(from: InvConfig_RestrictSw): string {
        let content: string;

        content = content + from.id;
        content = content + from.onExecCtrl;
        content = content + from.toDisplayMessage;
        content = content + from.message;

        return content;
    }

    static GetNew(from: InvConfig_RestrictSw): InvConfig_RestrictSw {
        let to: InvConfig_RestrictSw = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: InvConfig_RestrictSw): boolean {
        if (to.id !== this.id)
            return false;
        if (to.onExecCtrl !== this.onExecCtrl)
            return false;
        if (to.toDisplayMessage !== this.toDisplayMessage)
            return false;
        if (to.message !== this.message)
            return false;

        return true;
    }

    clone(): InvConfig_RestrictSw {
        let copied: InvConfig_RestrictSw = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
