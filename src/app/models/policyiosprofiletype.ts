﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {ModelBase_POCO, ModelBase} from "../shared/model-base";

export class PolicyIosProfileType extends ModelBase {

    constructor() {
        super();
    }

    identifier: string;
    notUninstallProfile: boolean;
    regAt: Date;
    fileHash: string;
    note: string;
    settingName: string;
    filename: string;
    ipcuProfile: string;
    fileId: string;

    equals(to: PolicyIosProfileType): boolean {
        if (to.identifier !== this.identifier)
            return false;
        if (to.notUninstallProfile !== this.notUninstallProfile)
            return false;
        if (to.regAt !== this.regAt)
            return false;
        if (to.fileHash !== this.fileHash)
            return false;
        if (to.note !== this.note)
            return false;
        if (to.settingName !== this.settingName)
            return false;
        if (to.filename !== this.filename)
            return false;
        if (to.ipcuProfile !== this.ipcuProfile)
            return false;
        if (to.fileId !== this.fileId)
            return false;

        return true;
    }

    clone(): PolicyIosProfileType {
        let copied = new PolicyIosProfileType();

        copied.identifier = this.identifier;
        copied.notUninstallProfile = this.notUninstallProfile;
        copied.regAt = this.regAt;
        copied.fileHash = this.fileHash;
        copied.note = this.note;
        copied.settingName = this.settingName;
        copied.filename = this.filename;
        copied.ipcuProfile = this.ipcuProfile;
        copied.fileId = this.fileId;

        return copied;
    }

    static getNew(from: PolicyIosProfileType): PolicyIosProfileType {
        let to = from.clone();

        return to;
    }
}
