﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

/// <summary>
/// Group user policy change allows
/// </summary>
export class Config_GroupPolicy extends ModelBase_POCO {

    id: string;
    oId: string;
    onGroupPolicy: boolean;     // To allow group admin to change
    onPcDiag: boolean;     // To allow group admin to change
    onPcCollect: boolean;     // To allow group admin to change
    onPcCtrl: boolean;     // To allow group admin to change
    onMobileDiag: boolean;     // To allow group admin to change
    onMobileCollect: boolean;     // To allow group admin to change
    onMobileCtrl: boolean;     // To allow group admin to change
    audit: AuditType;

    static GetHash(from: Config_GroupPolicy): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.onGroupPolicy;
        content = content + from.onPcDiag;
        content = content + from.onPcCollect;
        content = content + from.onPcCtrl;
        content = content + from.onMobileDiag;
        content = content + from.onMobileCollect;
        content = content + from.onMobileCtrl;

        return content;
    }

    static GetNew(from: Config_GroupPolicy): Config_GroupPolicy {
        let to: Config_GroupPolicy = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: Config_GroupPolicy): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.onGroupPolicy !== this.onGroupPolicy)
            return false;
        if (to.onPcDiag !== this.onPcDiag)
            return false;
        if (to.onPcCollect !== this.onPcCollect)
            return false;
        if (to.onPcCtrl !== this.onPcCtrl)
            return false;
        if (to.onMobileDiag !== this.onMobileDiag)
            return false;
        if (to.onMobileCollect !== this.onMobileCollect)
            return false;
        if (to.onMobileCtrl !== this.onMobileCtrl)
            return false;

        return true;
    }

    clone(): Config_GroupPolicy {
        let copied: Config_GroupPolicy = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
