﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

/// <summary>
/// Hw item details (option list)
/// </summary>
export class InvConfig_AssetInfoOption extends ModelBase_POCO {

    id: string;     // Unique(OId+ItemId+OptionNo)
    oId: string;
    itemId: number;
    optionNo: number;
    nm: string;     // Name
    audit: AuditType;

    static GetHash(from: InvConfig_AssetInfoOption): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.itemId;
        content = content + from.optionNo;
        content = content + from.nm;

        return content;
    }

    static GetNew(from: InvConfig_AssetInfoOption): InvConfig_AssetInfoOption {
        let to: InvConfig_AssetInfoOption = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: InvConfig_AssetInfoOption): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.itemId !== this.itemId)
            return false;
        if (to.optionNo !== this.optionNo)
            return false;
        if (to.nm !== this.nm)
            return false;

        return true;
    }

    clone(): InvConfig_AssetInfoOption {
        let copied: InvConfig_AssetInfoOption = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
