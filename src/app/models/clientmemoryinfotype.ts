﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {ModelBase_POCO, ModelBase} from "../shared/model-base";

export class ClientMemoryInfoType extends ModelBase {

    constructor() {
        super();
    }

    physicalSize: number;
    virtualSize: number;
    freeSize: number;

    equals(to: ClientMemoryInfoType): boolean {
        if (to.physicalSize !== this.physicalSize)
            return false;
        if (to.virtualSize !== this.virtualSize)
            return false;
        if (to.freeSize !== this.freeSize)
            return false;

        return true;
    }

    clone(): ClientMemoryInfoType {
        let copied = new ClientMemoryInfoType();

        copied.physicalSize = this.physicalSize;
        copied.virtualSize = this.virtualSize;
        copied.freeSize = this.freeSize;

        return copied;
    }

    static getNew(from: ClientMemoryInfoType): ClientMemoryInfoType {
        let to = from.clone();

        return to;
    }
}
