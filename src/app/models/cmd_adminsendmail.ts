﻿// Auto Generated by Samil Lee with Devart Entity Developer For QualitySoft

import {GeneralHelper as GH} from '../shared/general-helper';
import {ModelBase_POCO} from '../shared/model-base';

// Classes used : Relation, Complex type, Json type
import {AuditType} from './audittype';

/// <summary>
/// Mail send requests to client admin
/// </summary>
export class Cmd_AdminSendMail extends ModelBase_POCO {

    id: string;     // Log UUID
    oId: string;
    adminMailOnCode: string;     // See code table
    cId: string;
    message: string;
    audit: AuditType;

    static GetHash(from: Cmd_AdminSendMail): string {
        let content: string;

        content = content + from.id;
        content = content + from.oId;
        content = content + from.adminMailOnCode;
        content = content + from.cId;
        content = content + from.message;

        return content;
    }

    static GetNew(from: Cmd_AdminSendMail): Cmd_AdminSendMail {
        let to: Cmd_AdminSendMail = JSON.parse(JSON.stringify(from));

        to.isNew = true;
        to.id = GH.GenerateUuid();
        return to;
    }

    constructor() {
        super();
        this.id = GH.GenerateUuid();
        this.isNew = true;
    }

    equals(to: Cmd_AdminSendMail): boolean {
        if (to.id !== this.id)
            return false;
        if (to.oId !== this.oId)
            return false;
        if (to.adminMailOnCode !== this.adminMailOnCode)
            return false;
        if (to.cId !== this.cId)
            return false;
        if (to.message !== this.message)
            return false;

        return true;
    }

    clone(): Cmd_AdminSendMail {
        let copied: Cmd_AdminSendMail = JSON.parse(JSON.stringify(this));

        return copied;
    }

}
